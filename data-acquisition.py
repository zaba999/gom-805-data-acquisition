import serial
from optparse import OptionParser
import time
import msvcrt

parser = OptionParser()
parser.add_option("-o", "--output-file", dest="filename",
                  help="write output to CSV, and log", metavar="FILE")
parser.add_option("-p", "--serial-port", dest="port",
                  help="device serial port")
parser.add_option("-n", "--num-samples", dest="num_samp",
                  help="number of samples to collect", default=0)
parser.add_option("-f", "--frequency", dest="frequency",
                  help="frequency of reading data from device (max = 60)", default=1)

(options, args) = parser.parse_args()

filename = options.filename
if options.filename == None:
    print('No output file name added')
#    filename = 'out'
    exit()

port = options.port
if options.port == None:
    print('No serial port added')
#    port = 'COM16'
    exit()

num_samp = options.num_samp

if(options.frequency == 0):
    print('Errror frequency can not be 0')
    exit()
samp_time_s = (1/float(options.frequency))

ser = serial.Serial(port, 115200, serial.EIGHTBITS, serial.PARITY_NONE, serial.STOPBITS_ONE, 1000)
#ser.open()
ser.write(b'*idn?\r\n')
first_line = ser.read_until()

log_file = open(filename+".log","a+")
out_file = open(filename+".csv","w+", buffering=1)

log_file.write('data-acquisition.py\n')
start_time = time.time()
log_file.write('start at: %s\n'% (time.asctime( time.localtime(start_time))))
print('start at: %s\n'% (time.asctime( time.localtime(start_time))))
log_file.write('device: %s\n'% (first_line))
print('device at: %s\n'% (first_line))
log_file.write('output filename: %s\n'% (filename))
print('output filename: %s\n'% (filename))
log_file.write('serial port: %s\n'% (port))
print('serial port: %s\n'% (port))
log_file.write('number of samples to collect: %d\n'% (num_samp))
print('number of samples to collect: %d\n'% (num_samp))
log_file.write('sampling frequency: %f\n'% (float(options.frequency)))
print('sampling frequency: %f\n'% (float(options.frequency)))

max_len_glob = 10*float(options.frequency)
global_point = 0

def run_once(g_m):
    cur_time = time.time() + samp_time_s
    ser.write(b'read?\r\n')
    str_val = ser.read_until() 
    #val = float(str_val)
    time_mesure = time.time()-start_time
    out_file.write('%f,%s,%d\n'%(time_mesure, str_val, g_m))
    sleep_time = cur_time - time.time()
    if sleep_time > 0.0:
        time.sleep(sleep_time)

if num_samp > 0:
    while num_samp > 0:
        run_once(global_point)
        num_samp -= 1
        
else:
    while True:
        run_once(global_point)
        if msvcrt.kbhit():
            s_char = msvcrt.getch().decode('utf-8')
            if s_char == 'm':
                global_point += 1
                print(global_point)
            if s_char == 'q':
                break

log_file.write('end of run\n\n')
log_file.close()
out_file.close()
ser.close()
